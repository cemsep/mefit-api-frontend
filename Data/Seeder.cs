﻿using MeFitAPIFrontend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitAPIFrontend.Data
{
    public static class Seeder
    {
        public async static Task SeedUsers(MeFitContext context)
        {
            Console.WriteLine("Seeding users...");

            context.Users.Add(new User()
            {
                Password = "password1",
                FirstName = "Jesse",
                LastName = "Evans",
                IsContributor = true,
                IsAdmin = false
            });
            context.Users.Add(new User()
            {
                Password = "password2",
                FirstName = "Paul",
                LastName = "Thomas",
                IsContributor = true,
                IsAdmin = false
            });
            context.Users.Add(new User()
            {
                Password = "password3",
                FirstName = "Jerry",
                LastName = "Johnson",
                IsContributor = false,
                IsAdmin = true
            });

            await context.SaveChangesAsync();
        }

        public async static Task SeedAddresses(MeFitContext context)
        {
            Console.WriteLine("Seeding addresses...");

            context.Addresses.Add(new Address()
            {
                AddressLine1 = "404 Queens Lane",
                AddressLine2 = "",
                AddressLine3 = "",
                PostalCode = 78588,
                City = "San Isidro",
                Country = "USA"
            });
            context.Addresses.Add(new Address()
            {
                AddressLine1 = "3006 School House Road",
                AddressLine2 = "",
                AddressLine3 = "",
                PostalCode = 39140,
                City = "New Hebron",
                Country = "USA"
            });
            context.Addresses.Add(new Address()
            {
                AddressLine1 = "4012 Ingram Street",
                AddressLine2 = "",
                AddressLine3 = "",
                PostalCode = 08214,
                City = "Dennisville",
                Country = "USA"
            });

            await context.SaveChangesAsync();
        }

        public async static Task SeedPrograms(MeFitContext context)
        {
            Console.WriteLine("Seeding programs...");

            context.Add(new Models.Program()
            {
                Name = "Beginners Program",
                Catagory = "Full body workout"
            });
            context.Add(new Models.Program()
            {
                Name = "Intermediate Program",
                Catagory = "Full body workout"
            });
            context.Add(new Models.Program()
            {
                Name = "Advanced Program",
                Catagory = "Full body workout"
            });

            await context.SaveChangesAsync();
        }

        public async static Task SeedWorkouts(MeFitContext context)
        {
            Console.WriteLine("Seeding workouts...");

            context.Add(new Workout()
            {
                Name = "Monday Workout",
                Type = "Upper body",
                Complete = false
            });
            context.Add(new Workout()
            {
                Name = "Wednesday Workout",
                Type = "Lower body",
                Complete = false
            });
            context.Add(new Workout()
            {
                Name = "Friday Workout",
                Type = "Arms",
                Complete = false
            });

            await context.SaveChangesAsync();
        }

        public async static Task SeedProgramWorkouts(MeFitContext context)
        {
            Console.WriteLine("Seeding program workouts...");

            context.Add(new ProgramWorkout()
            {
                WorkoutId = 1,
                ProgramId = 1
            });
            context.Add(new ProgramWorkout()
            {
                WorkoutId = 1,
                ProgramId = 2
            });
            context.Add(new ProgramWorkout()
            {
                WorkoutId = 1,
                ProgramId = 3
            });

            await context.SaveChangesAsync();
        }

        public async static Task SeedGoals(MeFitContext context)
        {
            Console.WriteLine("Seeding goals...");

            context.Add(new Goal()
            {
                Description = "Default description",
                EndDate = new DateTime(2020, 04, 20),
                Achieved = false,
                ProgramId = 1
            });

            await context.SaveChangesAsync();
        }

        public async static Task SeedGoalWorkouts(MeFitContext context)
        {
            Console.WriteLine("Seeding goal workouts...");

            context.Add(new GoalWorkout()
            {
                EndDate = new DateTime(2020, 04, 20),
                WorkoutId = 1,
                GoalId = 1
            }); ;

            await context.SaveChangesAsync();
        }

        public async static Task SeedExercises(MeFitContext context)
        {
            Console.WriteLine("Seeding exercises...");

            context.Add(new Exercise()
            {
                Name = "Bench press",
            });
            context.Add(new Exercise()
            {
                Name = "Pull-up",
            });
            context.Add(new Exercise()
            {
                Name = "Overhead press",
            });
            context.Add(new Exercise()
            {
                Name = "Squat",
            });
            context.Add(new Exercise()
            {
                Name = "Deadlift",
            });
            context.Add(new Exercise()
            {
                Name = "Bulgarian split squat",
            });
            context.Add(new Exercise()
            {
                Name = "Barbell biceps curl",
            });
            context.Add(new Exercise()
            {
                Name = "Cable tricep pushdown",
            });
            context.Add(new Exercise()
            {
                Name = "Skull crusher",
            });

            await context.SaveChangesAsync();
        }

        public async static Task SeedSets(MeFitContext context)
        {
            Console.WriteLine("Seeding sets...");

            context.Add(new Set()
            {
                ExerciseRepititions = 12,
                ExerciseId = 1,
                WorkoutId = 1
            });
            context.Add(new Set()
            {
                ExerciseRepititions = 12,
                ExerciseId = 2,
                WorkoutId = 1
            });
            context.Add(new Set()
            {
                ExerciseRepititions = 12,
                ExerciseId = 3,
                WorkoutId = 1
            });
            context.Add(new Set()
            {
                ExerciseRepititions = 12,
                ExerciseId = 4,
                WorkoutId = 2
            });
            context.Add(new Set()
            {
                ExerciseRepititions = 12,
                ExerciseId = 5,
                WorkoutId = 2
            });
            context.Add(new Set()
            {
                ExerciseRepititions = 12,
                ExerciseId = 6,
                WorkoutId = 2
            });
            context.Add(new Set()
            {
                ExerciseRepititions = 12,
                ExerciseId = 7,
                WorkoutId = 3
            });
            context.Add(new Set()
            {
                ExerciseRepititions = 12,
                ExerciseId = 8,
                WorkoutId = 3
            });
            context.Add(new Set()
            {
                ExerciseRepititions = 12,
                ExerciseId = 9,
                WorkoutId = 3
            });

            await context.SaveChangesAsync();
        }

        public async static Task SeedDatabaseStatuses(MeFitContext context) {

            Console.Write("Seeding statuses...");

            context.Add(new DatabaseStatus() 
            {
                IsSeeded = true
            });

            await context.SaveChangesAsync();
        }
    }
}
