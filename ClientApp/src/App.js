import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';

import './custom.css'
import { SeedData } from './components/SeedData';
import { CreateProfile } from './components/CreateProfile';
import { CreateProfileConfirmation } from './components/CreateProfileConfirmation';
import { AllProfiles } from './components/AllProfiles';
import { EditProfile } from './components/EditProfile';
import { DeleteProfile } from './components/DeleteProfile';
import { CreateExercise } from './components/CreateExercise';
import { CreateExerciseConfirmation } from './components/CreateExerciseConfirmation';
import { AllExercises } from './components/AllExercises';
import { EditExercise } from './components/EditExercise';
import { DeleteExercise } from './components/DeleteExercise';


export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/create-profile' component={CreateProfile} />
        <Route path='/create-profile-confirmation' component={CreateProfileConfirmation} />
        <Route path='/all-profiles' component={AllProfiles} />
        <Route path='/edit-profile/:id' component={EditProfile} />
        <Route path='/delete-profile/:id' component={DeleteProfile} />
        <Route path='/create-exercise' component={CreateExercise} />
        <Route path='/create-exercise-confirmation' component={CreateExerciseConfirmation} />
        <Route path='/all-exercises' component={AllExercises} />
        <Route path='/edit-exercise/:id' component={EditExercise} />
        <Route path='/delete-exercise/:id' component={DeleteExercise} />
        <Route path='/seed-data' component={SeedData} />
      </Layout>
    );
  }
}
