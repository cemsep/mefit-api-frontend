import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import './AllExercises.css';

export class AllExercises extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: true, exercises: [] };
    }

    componentDidMount() {
        this.asyncLoad().then(data => {
            this.setState({ loading: false, exercises: data });
        });
    }

    asyncLoad() {
        return new Promise((resolve, reject) => {
            fetch('api/exercise').then(response => response.json())
                .then(data => resolve(data));
        });
    }

    render() {

        const { loading } = this.state;

        if (loading) {
            return (
                <div>Loading exercises...</div>
            );
        }

        if (!this.state.exercises.length) {
            return (
                <div>There are no exercises registered at this point.</div>
            );
        }

        const exercises = [];

        for (const [index, value] of this.state.exercises.entries()) {
            exercises.push(<tr key={index}>
                <th scope="row">{value.id}</th>
                <td>{value.name}</td>
                <td>{value.description ? value.description : "NULL"}</td>
                <td>{value.targetMuscleGroup ? value.targetMuscleGroup : "NULL"}</td>
                <td className="w-25">{value.image ? <img src={value.image} className="img-fluid img-thumbnail" alt="exercise"></img> : "NULL"}</td>
                <td>{value.vidLink ? <a href={value.vidLink } target="_blank" rel="noopener noreferrer">Video link</a>: "NULL"}</td>
                <td><Link to={{ pathname: `/edit-exercise/${value.id}`, state: {value} }}>Edit</Link></td>
                <td><Link to={{ pathname: `/delete-exercise/${value.id}`, state: {value} }}>Delete</Link></td>
            </tr>);
        }

        return (
            <div>
                <table className="table table-image">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Target Muscle Group</th>
                            <th scope="col">Image</th>
                            <th scope="col">Video Link</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {exercises}
                    </tbody>
                </table>
            </div>
        );

    }
}
export default withRouter(AllExercises)

