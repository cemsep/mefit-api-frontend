﻿import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';

export class DeleteProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentProfileId: null,
            currentProfileGoalId: null 
        };
    }

    componentDidMount() {
        this.setState({ currentProfileId: this.props.location.state.value.id, 
            currentProfileGoalId: this.props.location.state.value.goalId })
    }

    deleteProfile() {
        if(this.state.currentProfileGoalId) {
            return new Promise((resolve, reject) => {
                fetch(`api/profile/${this.state.currentProfileId}`, {
                    method: 'DELETE'
                })
                .then(response => response.json())
                .then(data => resolve(data))
            }).then((data) => {
                return new Promise((resolve, reject) => {
                    fetch(`api/goal/${this.state.currentProfileGoalId}`, {
                        method: 'DELETE'
                    })
                    .then(response => response.json())
                    .then(data => resolve(data))
                })
                .then(data => {
                    this.props.history.push('/all-profiles');
                })
            });
        }
        else {
            return new Promise((resolve, reject) => {
                fetch(`api/profile/${this.state.currentProfileId}`, {
                    method: 'DELETE'
                })
                .then(response => response.json())
                .then(data => resolve(data))
            }).then((data) => {
                this.props.history.push('/all-profiles');
            });
        }
    }

    render() {

        var styles1 = {
            marginBottom: '3em'
        }

        var styles2 = {
            marginRight: '2em'
        };

        return (
            <div style={styles1}>
                <h1>Delete profile</h1>
                <br/>
                <h3>Are you sure you want to delete this?</h3>
                <br/>
                <div>
                    <h4>Profile</h4>
                    <hr />
                    <dl className="row">
                        <dt className="col-sm-2">
                            UserId
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.userId}
                        </dd>
                        <dt className="col-sm-2">
                            GoalId
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.goalId ? this.props.location.state.value.goalId : "NULL"}
                        </dd>
                        <dt className="col-sm-2">
                            AddressId
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.addressId ? this.props.location.state.value.addressId : "NULL"}
                        </dd>
                        <dt className="col-sm-2">
                            ProgramId
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.programId ? this.props.location.state.value.programId : "NULL"}
                        </dd>
                        <dt className="col-sm-2">
                            WorkoutId
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.workoutId ? this.props.location.state.value.workoutId : "NULL"}
                        </dd>
                        <dt className="col-sm-2">
                            SetId
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.setId ? this.props.location.state.value.setId : "NULL"}
                        </dd>
                        <dt className="col-sm-2">
                            Weight
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.weight}
                        </dd>
                        <dt className="col-sm-2">
                            Height
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.height}
                        </dd>
                        <dt className="col-sm-2">
                            Medical Conditions
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.medicalConditions ? this.props.location.state.value.medicalConditions : "NULL"}
                        </dd>
                        <dt className="col-sm-2">
                            Disabilities
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.disabilities ? this.props.location.state.value.disabilities : "NULL"}
                        </dd>
                    </dl>

                    <div>
                        <button type="button" className="btn btn-danger" style={styles2} onClick={this.deleteProfile.bind(this)}>Delete</button>
                        <Link to="/all-profiles">Back to list</Link>
                    </div>
                </div>
            </div>
        );

    }
}

export default withRouter(DeleteProfile)
