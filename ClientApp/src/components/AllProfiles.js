﻿import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';

export class AllProfiles extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: true, profiles: [] };
    }

    componentDidMount() {
        this.asyncLoad().then(data => {
            this.setState({ loading: false, profiles: data });
        });
    }

    asyncLoad() {
        return new Promise((resolve, reject) => {
            fetch('api/profile').then(response => response.json())
                .then(data => resolve(data));
        });
    }

    render() {

        const { loading } = this.state;

        if (loading) {
            return (
                <div>Loading profiles...</div>
            );
        }

        if (!this.state.profiles.length) {
            return (
                <div>There are no profiles registered at this point.</div>
            );
        }

        const profiles = [];

        for (const [index, value] of this.state.profiles.entries()) {
            profiles.push(<tr key={index}>
                <th scope="row">{value.id}</th>
                <td>{value.userId}</td>
                <td>{value.goalId ? value.goalId : "NULL"}</td>
                <td>{value.addressId ? value.addressId : "NULL"}</td>
                <td>{value.programId ? value.programId : "NULL"}</td>
                <td>{value.workoutId ? value.workoutId : "NULL"}</td>
                <td>{value.setId ? value.setId : "NULL"}</td>
                <td>{value.weight}</td>
                <td>{value.height}</td>
                <td>{value.medicalConditions ? value.medicalConditions : "NULL"}</td>
                <td>{value.disabilities ? value.disabilities : "NULL"}</td>
                <td><Link to={{ pathname: `/edit-profile/${value.id}`, state: {value} }}>Edit</Link></td>
                <td><Link to={{ pathname: `/delete-profile/${value.id}`, state: {value} }}>Delete</Link></td>
            </tr>);
        }

        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">UserId</th>
                            <th scope="col">GoalId</th>
                            <th scope="col">AddressId</th>
                            <th scope="col">ProgramId</th>
                            <th scope="col">WorkoutId</th>
                            <th scope="col">SetId</th>
                            <th scope="col">Weight</th>
                            <th scope="col">Height</th>
                            <th scope="col">Medical Conditions</th>
                            <th scope="col">Disabilities</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {profiles}
                    </tbody>
                </table>
            </div>
        );

    }
}
export default withRouter(AllProfiles)

