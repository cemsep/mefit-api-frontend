import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

export class CreateExercise extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imageLabel: 'Image',
            name: null,
            description: null,
            targetMuscleGroup: null,
            image: null,
            vidLink: null
        };
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name

        this.setState({ [name]: value });
    }

    submitForm() {
      return new Promise((resolve, reject) => {
        fetch('api/exercise', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                name: this.state.name,
                description: this.state.description,
                targetMuscleGroup: this.state.targetMuscleGroup,
                image: this.state.image,
                vidLink: this.state.vidLink
            })
        })
        .then(response => response.json())
        .then(data => {
            resolve(data)
        });
      }).then(data => {
        
          this.props.history.push('/create-exercise-confirmation', {
              id: data.id,
              name: data.name,
              description: data.description,
              targetMuscleGroup: data.targetMuscleGroup,
              image: data.image,
              vidLink: data.vidLink
          });
      });
    }

    render() {

        var styles = {
            marginBottom: '3em'
        }

        return (
            <div style={styles}>
                <h1>Create exercise</h1>

                <p>Fill in and submit the form to create an exercise.</p>

                <form>
                    <div className="form-group">
                        <label htmlFor="formGroupName">Name</label>
                        <input type="text" className="form-control" id="formGroupName" name="name" onChange={this.handleInputChange.bind(this)} required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupDescription">Description</label>
                        <input type="text" className="form-control" id="formGroupDescription" name="description" onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupTargetMuscleGroup">Target Muscle Group</label>
                        <input type="text" className="form-control" id="formGroupTargetMuscleGroup" name="targetMuscleGroup" onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupImage">Image Link</label>
                        <input type="text" className="form-control" id="formGroupImage" name="image" onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupVidLink">Video Link</label>
                        <input type="text" className="form-control" id="formGroupVidLink" name="vidLink" onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <button className="btn btn-primary" type="button" onClick={this.submitForm.bind(this)}>Submit form</button>
                </form>
            </div>
        );
    }
}

export default withRouter(CreateExercise)
