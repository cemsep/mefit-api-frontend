﻿import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

export class CreateProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            availableIds: [1, 2, 3],
            addresses: [],
            userId: null,
            addressId: null, 
            weight: null,
            height: null,
            medicalConditions: null,
            disabilities: null,
            goalDescription: null,
            goalEndDate: null,
            goalAchieved: 0
        };
    }

    componentDidMount() {
        this.asyncLoadProfiles().then(data => {
            let { availableIds } = this.state;
            data.forEach(profile => {
                var index = availableIds.indexOf(profile.userId);
                availableIds.splice(index, 1);
            });
            this.setState({ availableIds: availableIds });
            this.asyncLoadAddresses().then(data => {
                this.setState({ loading: false, addresses: data })
            });
        });
    }

    asyncLoadProfiles() {
        return new Promise((resolve, reject) => {
            fetch('api/profile')
            .then(response => response.json())
            .then(data => resolve(data));
        });
    }

    asyncLoadAddresses() {
        return new Promise((resolve, reject) => {
            fetch('api/address')
            .then(response => response.json())
            .then(data => resolve(data));
        });
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name

        if (name === 'goalAchieved') {
            if (value === 'true')
                this.setState({ [name]: 1 });
            else
                this.setState({ [name]: 0 });
        } else {
            this.setState({ [name]: value });
        }
    }

    submitForm() {
        if(this.state.goalDescription && this.state.goalEndDate) {
            return new Promise((resolve, reject) => {
                fetch('api/goal', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        description: this.state.goalDescription,
                        endDate: this.state.goalEndDate,
                        achieved: this.state.goalAchieved
                    })
                })
                .then(response => response.json())
                .then(data => resolve(data));
            })
            .then(data => {
                return new Promise((resolve, reject) => {
                    fetch('api/profile', {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({
                            userId: parseInt(this.state.userId),
                            goalId: data.id,
                            addressId: parseInt(this.state.addressId),
                            weight: parseFloat(this.state.weight),
                            height: parseFloat(this.state.height),
                            medicalConditions: this.state.medicalConditions,
                            disabilities: this.state.disabilities
                        })
                    })
                    .then(response => response.json())
                    .then(data => {
                        resolve(data)
                    });
                }).then(data => {
                    this.props.history.push('/create-profile-confirmation', {
                        id: data.id,
                        userId: data.userId,
                        weight: data.weight,
                        height: data.height,
                        medicalConditions: data.medicalConditions,
                        disabilities: data.disabilities
                    });
                });
            })
        } else {
            return new Promise((resolve, reject) => {
                fetch('api/profile', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        userId: parseInt(this.state.userId),
                        addressId: parseInt(this.state.addressId),
                        weight: parseFloat(this.state.weight),
                        height: parseFloat(this.state.height),
                        medicalConditions: this.state.medicalConditions,
                        disabilities: this.state.disabilities
                    })
                })
                .then(response => response.json())
                .then(data => {
                    resolve(data)
                });
            }).then(data => {
                this.props.history.push('/create-profile-confirmation', {
                    id: data.id,
                    userId: data.userId,
                    weight: data.weight,
                    height: data.height,
                    medicalConditions: data.medicalConditions,
                    disabilities: data.disabilities
                });
            });
        }  
    }

    render() {

        const { loading } = this.state;

        if (loading) {
            return (
                <div>Loading...</div>
            );
        }

        const options = [];

        for (const [index, value] of this.state.availableIds.entries()) {
            options.push(<option key={index}>{value}</option>)
        }

        const addresses = [];

        for (const [index, value] of this.state.addresses.entries()) {
            addresses.push(<option key={index} value={value.id}>{value.addressLine1} 
                {value.addressLine2} 
                {value.addressLine3}, 
                {value.postalCode}, 
                {value.city}, 
                {value.country}</option>)
        }

        var styles = {
            marginBottom: '3em'
        }

        return (
            <div style={styles}>
                <h1>Create profile</h1>

                <p>Fill in and submit the form to create a profile.</p>

                <form>
                    <div className="form-group">
                        <label htmlFor="formGroupUserId">User ID</label>
                        <select className="form-control" id="formGroupUserId" name="userId" onChange={this.handleInputChange.bind(this)}
                            required={this.state.availableIds.length ? true : null}>
                            <option value="">Available IDs</option>
                            {options}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupAddress">Address</label>
                        <select className="form-control" id="formGroupAddress" name="addressId" onChange={this.handleInputChange.bind(this)}
                            required={this.state.addresses.length ? true : null}>
                            <option value="">Available Addresses</option>
                            {addresses}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupWeight">Weight</label>
                        <input type="text" className="form-control" id="formGroupWeight" name="weight" onChange={this.handleInputChange.bind(this)} required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupHeight">Height</label>
                        <input type="text" className="form-control" id="formGroupHeight" name="height" onChange={this.handleInputChange.bind(this)} required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupMedicalConditions">Medical Conditions</label>
                        <input type="text" className="form-control" id="formGroupMedicalConditions" name="medicalConditions" onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupDisabilities">Disabilities</label>
                        <input type="text" className="form-control" id="formGroupDisabilities" name="disabilities" onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupGoalDescription">Goal Description</label>
                        <input type="text" className="form-control" id="formGroupGoalDescription" name="goalDescription" onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupGoalEndDate">Goal End Date</label>
                        <input type="date" className="form-control" id="formGroupGoalEndDate" name="goalEndDate" onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupGoalAchieved">Goal Achieved</label>
                        <select className="form-control" id="formGroupGoalAchieved" name="goalAchieved" onChange={this.handleInputChange.bind(this)} defaultValue="false">
                            <option value="false">No</option>
                            <option value="true">Yes</option>
                        </select>
                    </div>
                    <button className="btn btn-primary" type="button" onClick={this.submitForm.bind(this)}>Submit form</button>
                </form>
            </div>
        );
    }
}

export default withRouter(CreateProfile)
