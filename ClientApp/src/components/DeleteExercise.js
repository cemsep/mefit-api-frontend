import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';

export class DeleteExercise extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentExerciseId: null };
    }

    componentDidMount() {
        this.setState({ currentExerciseId: this.props.location.state.value.id })
    }

    deleteExercise() {
        return new Promise((resolve, reject) => {
            fetch(`api/exercise/${this.state.currentExerciseId}`, {
                method: 'DELETE'
            })
            .then(response => response.json())
            .then(data => resolve(data))
        }).then((data) => {
            this.props.history.push('/all-exercises');
        });
    }

    render() {

        var styles1 = {
            marginBottom: '3em'
        }

        var styles2 = {
            marginRight: '2em'
        };

        return (
            <div style={styles1}>
                <h1>Delete exercise</h1>
                <br/>
                <h3>Are you sure you want to delete this?</h3>
                <br/>
                <div>
                    <h4>Exercise</h4>
                    <hr />
                    <dl className="row">
                        <dt className="col-sm-2">
                            Name
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.name}
                        </dd>
                        <dt className="col-sm-2">
                            Description
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.description ? this.props.location.state.value.description : "NULL"}
                        </dd>
                        <dt className="col-sm-2">
                            Target Muscle Group
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.targetMuscleGroup ? this.props.location.state.value.targetMuscleGroup : "NULL"}
                        </dd>
                        <dt className="col-sm-2">
                            Image
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.image ? <img src={this.props.location.state.value.image} alt="exercise" style={{ maxWidth: '30%', height: 'auto' }}></img> : "NULL"}
                        </dd>
                        <dt className="col-sm-2">
                            Video Link
                        </dt>
                        <dd className="col-sm-10">
                            {this.props.location.state.value.vidLink ? this.props.location.state.value.vidLink : "NULL"}
                        </dd>
                    </dl>

                    <div>
                        <button type="button" className="btn btn-danger" style={styles2} onClick={this.deleteExercise.bind(this)}>Delete</button>
                        <Link to="/all-exercises">Back to list</Link>
                    </div>
                </div>
            </div>
        );

    }
}

export default withRouter(DeleteExercise)
