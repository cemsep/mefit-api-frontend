import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

export class EditExercise extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imageLabel: null,
            name: null,
            description: null,
            targetMuscleGroup: null,
            image: null,
            vidLink: null

        };
    }

    componentDidMount() {
      this.setState({
          currentExerciseId: this.props.location.state.value.id,
          name: this.props.location.state.value.name,
          description: this.props.location.state.value.description,
          targetMuscleGroup: this.props.location.state.value.targetMuscleGroup,
          image: this.props.location.state.value.image,
          vidLink: this.props.location.state.value.vidLink
      });
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name

        this.setState({ [name]: value });
    }

    submitForm() {
        return new Promise((resolve, reject) => {
            fetch(`api/exercise/${this.state.currentExerciseId}`, {
              method: 'PUT',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify({
                id: this.state.currentExerciseId,
                name: this.state.name,
                description: this.state.description,
                targetMuscleGroup: this.state.targetMuscleGroup,
                image: this.state.image,
                vidLink: this.state.vidLink
              })
            })
            .then(response => resolve())
        }).then(() => {
            this.props.history.push('/all-exercises');
        });
    }

    render() {

        const currentExercise = this.props.location.state.value;

        var styles1 = {
            marginBottom: '3em'
        }

        return (
            <div style={styles1}>
                <h1>Edit profile</h1>

                <p>Fill in and submit the form to edit an exercise.</p>

                <form>
                    <div className="form-group">
                        <label htmlFor="formGroupName">Name</label>
                        <input type="text" className="form-control" id="formGroupName" name="name" defaultValue={currentExercise.name} 
                            onChange={this.handleInputChange.bind(this)} required />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupDescription">Description</label>
                        <input type="text" className="form-control" id="formGroupDescription" name="description" defaultValue={currentExercise.description} 
                            onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupTargetMuscleGroup">Target Muscle Group</label>
                        <input type="text" className="form-control" id="formGroupTargetMuscleGroup" name="targetMuscleGroup" defaultValue={currentExercise.targetMuscleGroup} 
                            onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupImage">Image Link</label>
                        <input type="text" className="form-control" id="formGroupImage" name="image" defaultValue={currentExercise.image} 
                            onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="formGroupVidLink">Video Link</label>
                        <input type="text" className="form-control" id="formGroupVidLink" name="vidLink" defaultValue={currentExercise.vidLink} 
                            onChange={this.handleInputChange.bind(this)} />
                    </div>
                    <button className="btn btn-primary" type="button" onClick={this.submitForm.bind(this)}>Submit form</button>
                </form>
            </div>
        );
    }
}

export default withRouter(EditExercise)
