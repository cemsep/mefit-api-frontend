﻿import React, { Component } from 'react';

export class SeedData extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: true, isSeeded: false };
    }

    componentDidMount() {
        this.asyncLoad().then(data => {
            if(data.status === 200) {
                data.json().then(data => this.setState({ loading: false, isSeeded: data.isSeeded }));
            } else {
                this.setState({ loading: false });
            }
        });
    }

    asyncLoad() {
        return new Promise((resolve, reject) => {
            fetch('api/DatabaseStatus/1')
            .then(response => resolve(response));
        });
    }

    seedData() {
        return new Promise((resolve, reject) => {
            fetch('api/SeedDatabase', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: ''
            }).then(response => response);

            resolve();
        }).then(() => {
            this.setState({ isSeeded: true });
        }); 
    }

    render() {
        
        const { loading } = this.state;

        if (loading) {
            return (
                <div>Loading...</div>
            )
        }

        if (!this.state.isSeeded) {
            return (
                <div>
                    <h1>Seed data</h1>

                    <p>Click to button to seed dummy data to the database.</p>

                    <button className="btn btn-primary" onClick={this.seedData.bind(this)}>Seed</button>
                </div>
            );
        }
        else {
            return (
                <div>
                    <h1>Seed data</h1>

                    <p>Database has been seeded.</p>

                </div>
            );
        }
    }
}
