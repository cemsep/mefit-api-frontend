import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

export class CreateExerciseConfirmation extends Component {

    constructor(props) {
        super(props);
        this.state = { isNotFound: true };
    }

    componentDidMount() {
        if (this.props.location.state)
            this.setState({ isNotFound: false });
    }

    render() {

        const { isNotFound } = this.state;

        if (isNotFound) {
            return (
                <div>404 Not Found</div>
            );
        }

        return (
            <div>
                The exercise has been added. <br />
                <a href="/all-exercises">View all exercises</a>
            </div>
        );
        
    }
}

export default withRouter(CreateExerciseConfirmation)
