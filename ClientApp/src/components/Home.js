import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Home extends Component {
  static displayName = Home.name;

  render () {
      return (
          <div>
              <p>Welcome to MeFit API Frontend!</p>
              <Link to="/create-profile">Create profile</Link> &nbsp;&nbsp;
              <Link to="/all-profiles">All profiles</Link>
              <br />
              <Link to="/create-exercise">Create exercise</Link> &nbsp;&nbsp;
              <Link to="/all-exercises">All exercises</Link>
          </div>
      );
  }
}
