using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitAPIFrontend.Models
{
    public class DatabaseStatus
    {
        public int Id { get; set; }
        public bool IsSeeded { get; set; } = false;

    }
}