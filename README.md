# MeFit API Frontend

## Description

Extending MeFit Mini Case Web API to contain a front end framework Client. Using React

NB! Seed the data before creating anything.

## Contributors

Cem Pedersen     @cemsep     https://gitlab.com/cemsep

## Components

### CreateProfile

A component that stores a form of profile information and at submit, makes a API call to create/save a profile to database.

### CreateProfileConfirmation

Confirms that a profile is created and gives the user option to navigate to AllProfiles component.

### AllProfiles

A component that lists all profiles from the database. To each profile element, gives the user option to either edit or delete the profile.

### EditProfile

A component that stores a form of profile information and at submit, makes a API call to edit an existent profile in the database.

### DeleteProfile

A component that gives user the option to delete a profile from database.

### CreateExercise

A component that stores a form of exercise information and at submit, makes a API call to create/save an exercise to database.

### CreateExerciseConfirmation

Confirms that an exercise is created and gives the user option to navigate to AllExercises component.

### AllExercises

A component that lists all exercises from the database. To each exercise element, gives the user option to either edit or delete the exercise.

### EditExercise

A component that stores a form of exercise information and at submit, makes a API call to edit an existent exercise in the database.

### DeleteExercise

A component that gives user the option to delete an exercise from database.