﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeFitAPIFrontend.Data;
using MeFitAPIFrontend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MeFitAPIFrontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeedDatabaseController : ControllerBase
    {
        private readonly MeFitContext _context;

        public SeedDatabaseController(MeFitContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<ActionResult<string>> SeedAsync()
        {
            using (_context)
            {
                await Seeder.SeedUsers(_context);
                await Seeder.SeedAddresses(_context);
                await Seeder.SeedPrograms(_context);
                await Seeder.SeedWorkouts(_context);
                await Seeder.SeedProgramWorkouts(_context);
                await Seeder.SeedGoals(_context);
                await Seeder.SeedGoalWorkouts(_context);
                await Seeder.SeedExercises(_context);
                await Seeder.SeedSets(_context);
                await Seeder.SeedDatabaseStatuses(_context);
            }

            return "Database created";
        }
    }
}