﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MeFitAPIFrontend.Models;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPIFrontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly MeFitContext _context;

        public ProfileController(MeFitContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Profile>> GetProfiles()
        {
            return _context.Profiles;
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<Profile>> GetProfileById(int id)
        {
            return await _context.Profiles
                .Include(p => p.User)
                .Include(p => p.Address)
                .Include(p => p.Program)
                .Include(p => p.Workout)
                .Include(p => p.Set)
                .SingleOrDefaultAsync(p => p.Id == id);
        }

        [HttpPost]
        public async Task<ActionResult<Profile>> PostProfile(Profile profile)
        {
            try {
                await _context.Profiles.AddAsync(profile);
                await _context.SaveChangesAsync();
            } catch (Exception exc)
            {
                return BadRequest(exc);
            }

            return CreatedAtAction("GetProfileById", new Profile { Id = profile.Id }, profile);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Profile>> PutProfile(int id, Profile profile)
        {
            if (id != profile.Id)
            {
                return BadRequest();
            }
            _context.Entry(profile).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Profile>> DeleteProfile(int id)
        {
            Profile profile = await _context.Profiles.FindAsync(id);

            if (profile == null)
            {
                return NotFound();
            }

            _context.Profiles.Remove(profile);
            await _context.SaveChangesAsync();

            return profile;
        }
    }
}