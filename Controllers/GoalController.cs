﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MeFitAPIFrontend.Models;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPIFrontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GoalController : ControllerBase
    {
        private readonly MeFitContext _context;

        public GoalController(MeFitContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Goal>> GetGoal()
        {
            return _context.Goals;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Goal>> GetGoalById(int id)
        {
            return await _context.Goals.Include(goal => goal.GoalWorkouts).Where(goal => goal.Id == id).FirstOrDefaultAsync();
        }

        [HttpPost]
        public async Task<ActionResult<Goal>> PostGoal(Goal goal)
        {
            _context.Goals.Add(goal);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGoalById", new Goal { Id = goal.Id }, goal);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Goal>> PutGoal(int id, Goal goal)
        {
            if (id != goal.Id)
            {
                return BadRequest();
            }
            _context.Entry(goal).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Goal>> DeleteGoal(int id)
        {
            Goal goal = await _context.Goals.FindAsync(id);

            if (goal == null)
            {
                return NotFound();
            }

            _context.Goals.Remove(goal);
            await _context.SaveChangesAsync();

            return goal;
        }
    }
}