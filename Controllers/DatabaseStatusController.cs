using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeFitAPIFrontend.Data;
using MeFitAPIFrontend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MeFitAPIFrontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DatabaseStatusController : ControllerBase
    {
        private readonly MeFitContext _context;

        public DatabaseStatusController(MeFitContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<DatabaseStatus>> GetDatabaseStatuses()
        {
            return _context.DatabaseStatuses;
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<DatabaseStatus>> GetDatabaseStatusById(int id)
        {
            var status = await _context.DatabaseStatuses.FindAsync(id);

            if (status == null) {
                return NoContent();
            }

            return status;
            
        }
    }
}