﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MeFitAPIFrontend.Models;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPIFrontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        private readonly MeFitContext _context;

        public AddressController(MeFitContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Address>> GetAdresses()
        {
            return _context.Addresses;
        }


        [HttpGet("{id}")]
        public ActionResult<Address> GetAddressById(int id)
        {
            return _context.Addresses.Find(id);
        }

        [HttpPost]
        public ActionResult<Address> PostAddress(Address address)
        {
            _context.Addresses.Add(address);
            _context.SaveChanges();

            return CreatedAtAction("GetAddressById", new Address { Id = address.Id }, address);
        }

        [HttpPut("{id}")]
        public ActionResult<Address> PutAddress(int id, Address address)
        {
            if (id != address.Id)
            {
                return BadRequest();
            }
            _context.Entry(address).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult<Address> DeleteAddress(int id)
        {
            Address address = _context.Addresses.Find(id);

            if (address == null)
            {
                return NotFound();
            }

            _context.Addresses.Remove(address);
            _context.SaveChanges();

            return address;
        }
    }
}